---
title: 'spasmtools: an R package and application for optical trapping data generated from SPASM'
tags:
  - R
  - optical trapping
  - myosin
  - single molecule
authors:
  - name: Brent Scott
    orcid: 0000-0000-0000-0000
    affiliation: "1, 2" # (Multiple affiliations must be quoted)
  - name: Michael J. Greenberg
affiliations:
 - name: Department of Biochemistry and Molecular Biophysics, Washington University in St. Louis
   index: 1
date: 16 June 2023
bibliography: paper.bib
---

# Summary

SPASM is a computer program that analyses single molecule optical trapping data by identifying anomilies in the time series datasets produced by the QPD (quadrant photodiode detector) readout from optical trapping instruments. Here we introduce {spasmtools}, an R package which serves as a companion to the SPASM program which allows SPASM users to read SPASM data into R, post-process SPASM generated data, conduct further analyses, and generate publication quality figures from their data. Thus, the use of {spasmtools} provides an accelerated and reprodicible extension to the SPASM workflow.

# Statement of need

SPASM is gaining popularity as an open source tool to analyze optical trapping data. The data generated by SPASM provides individual event estimates of the underlying mechanics and kinetics of two proteins stochastically interacting. The use of SPASM undoubtebly removes a large burden from a optical trapper by providing automatic event detection software. However, the data generated from SPASM can contain descriptions of thousands of event measurements across several experimental conditions as well as ensemble averaged datasets. Performing customizations and reproducibility to post analyis fitting routines and plot customization is still burdensome to researchers. {spasmtools} is an R package and GUI application that provides access to an suite of SPASM friendly functions which accelerates and automates the post-processing of SPASM generated datasets. 


# Citations

Citations to entries in paper.bib should be in
[rMarkdown](http://rmarkdown.rstudio.com/authoring_bibliographies_and_citations.html)
format.

If you want to cite a software repository URL (e.g. something on GitHub without a preferred
citation) then you can do it with the example BibTeX entry below for @fidgit.

For a quick reference, the following citation commands can be used:
- `@author:2001`  ->  "Author et al. (2001)"
- `[@author:2001]` -> "(Author et al., 2001)"
- `[@author1:2001; @author2:2001]` -> "(Author1 et al., 2001; Author2 et al., 2002)"

# Figures

Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

Figure sizes can be customized by adding an optional second parameter:
![Caption for example figure.](figure.png){ width=20% }

# Acknowledgements

We would like to acknowledge and thanks the authors of the original SPASM software for creating and providing SPASM as an open source tools

# References
