{
  description = "A basic flake with a shell";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      rPkgs = with pkgs.rPackages; [data_table 
                                    dplyr 
                                    purrr 
                                    ggplot2 
                                    readxl 
                                    devtools 
                                    testthat 
                                    roxygen2 
                                    knitr
                                    usethis
                                    shiny];
    in {
      devShells.default = pkgs.mkShell {
        packages = [ pkgs.bashInteractive ];
        buildInputs = with pkgs; [R rPkgs];
      };
    });
}
